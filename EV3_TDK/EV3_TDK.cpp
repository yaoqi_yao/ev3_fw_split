// EV3_TDK.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <windows.h>
#define UBOOTSECTIONSIZE  0x050000
#define RAMDSECTIONOFFSET 0x250000

#define IH_MAGIC    0x27051956    /* Image Magic Number     */
#define IH_NMLEN    32            /* Image Name Length      */
typedef DWORD uint32_t;
typedef BYTE  uint8_t;


typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;


#define CRAMFS_MAGIC            0x28cd3d45      /* some random number */
#define CRAMFS_SIGNATURE        "Compressed ROMFS"

/*
* Width of various bitfields in struct cramfs_inode.
* Primarily used to generate warnings in mkcramfs.
*/
#define CRAMFS_MODE_WIDTH 16
#define CRAMFS_UID_WIDTH 16
#define CRAMFS_GID_WIDTH 8
#define CRAMFS_NAMELEN_WIDTH 6
#define CRAMFS_OFFSET_WIDTH 26

/*
* Since inode.namelen is a unsigned 6-bit number, the maximum cramfs
* path length is 63 << 2 = 252.
*/
#define CRAMFS_MAXPATHLEN (((1 << CRAMFS_NAMELEN_WIDTH) - 1) << 2)
#define CRAMFS_SIZE_WIDTH 24
/*
* Feature flags
*
* 0x00000000 - 0x000000ff: features that work for all past kernels
* 0x00000100 - 0xffffffff: features that don't work for past kernels
*/
#define CRAMFS_FLAG_FSID_VERSION_2      0x00000001      /* fsid version #2 */
#define CRAMFS_FLAG_SORTED_DIRS         0x00000002      /* sorted dirs */
#define CRAMFS_FLAG_HOLES               0x00000100      /* support for holes */
#define CRAMFS_FLAG_WRONG_SIGNATURE     0x00000200      /* reserved */
#define CRAMFS_FLAG_SHIFTED_ROOT_OFFSET 0x00000400      /* shifted root fs */
#define CRAMFS_FLAG_BLKSZ_MASK          0x00003800      /* Block size mask */
#define CRAMFS_FLAG_COMP_METHOD_MASK    0x0000C000      /* Compression method
* mask */

#define CRAMFS_FLAG_BLKSZ_SHIFT 11
#define CRAMFS_FLAG_COMP_METHOD_SHIFT 14

#define CRAMFS_FLAG_COMP_METHOD_NONE 0
#define CRAMFS_FLAG_COMP_METHOD_GZIP 1
#define CRAMFS_FLAG_COMP_METHOD_LZMA 2

/*
* Valid values in super.flags.  Currently we refuse to mount
* if (flags & ~CRAMFS_SUPPORTED_FLAGS).  Maybe that should be
* changed to test super.future instead.
*/
#define CRAMFS_SUPPORTED_FLAGS  ( 0x000000ff \
	| CRAMFS_FLAG_HOLES \
	| CRAMFS_FLAG_WRONG_SIGNATURE \
	| CRAMFS_FLAG_SHIFTED_ROOT_OFFSET \
	| CRAMFS_FLAG_BLKSZ_MASK \
	| CRAMFS_FLAG_COMP_METHOD_MASK)
#pragma  pack(1)

/*
* Reasonably terse representation of the inode data.
*/
struct cramfs_inode {
	u32 mode : CRAMFS_MODE_WIDTH, uid : CRAMFS_UID_WIDTH;
	/* SIZE for device files is i_rdev */
	u32 size : CRAMFS_SIZE_WIDTH, gid : CRAMFS_GID_WIDTH;
	/* NAMELEN is the length of the file name, divided by 4 and
	rounded up.  (cramfs doesn't support hard links.) */
	/* OFFSET: For symlinks and non-empty regular files, this
	contains the offset (divided by 4) of the file data in
	compressed form (starting with an array of block pointers;
	see README).  For non-empty directories it is the offset
	(divided by 4) of the inode of the first file in that
	directory.  For anything else, offset is zero. */
	u32 namelen : CRAMFS_NAMELEN_WIDTH, offset : CRAMFS_OFFSET_WIDTH;
};

struct cramfs_info {
	u32 crc;
	u32 edition;
	u32 blocks;
	u32 files;
};

/*
* Superblock information at the beginning of the FS.
*/
struct cramfs_super {
	u32 magic;                      /* 0x28cd3d45 - random number */
	u32 size;                       /* length in bytes */
	u32 flags;                      /* feature flags */
	u32 future;                     /* reserved for future use */
	u8 signature[16];               /* "Compressed ROMFS" */
	struct cramfs_info fsid;        /* unique filesystem info */
	u8 name[16];                    /* user-defined name */
	struct cramfs_inode root;       /* root inode data */
};


typedef struct image_header {
	uint32_t    ih_magic;         /* Image Header Magic Number */
	uint32_t    ih_hcrc;          /* Image Header CRC Checksum */
	uint32_t    ih_time;          /* Image Creation Timestamp  */
	uint32_t    ih_size;          /* Image Data Size           */
	uint32_t    ih_load;          /* Data     Load  Address    */
	uint32_t    ih_ep;            /* Entry Point Address       */
	uint32_t    ih_dcrc;          /* Image Data CRC Checksum   */
	uint8_t     ih_os;            /* Operating System          */
	uint8_t     ih_arch;          /* CPU architecture          */
	uint8_t     ih_type;          /* Image Type                */
	uint8_t     ih_comp;          /* Compression Type          */
	uint8_t     ih_name[IH_NMLEN];    /* Image Name            */
} image_header_t;

#pragma  pack()
static char  outputfiles[][32] = {
	"u_boot.bin",
	"uImage.bin",
	"ramdisk.bin",
};

static DWORD outputlen[] = {
	UBOOTSECTIONSIZE,
	UBOOTSECTIONSIZE,
	UBOOTSECTIONSIZE,
};
static DWORD swapdword(DWORD n)
{
	return (n << 24) | (n >> 24) | ((n >> 8) & 0xff00) | ((n << 8) & 0xff0000);
}

SECURITY_DESCRIPTOR sd;
static PACL BuildRestrictedSD(PSECURITY_DESCRIPTOR pSD);

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		for (int i = 0; i < argc;i++)
		{
			printf("%s\n", argv[i]);
		}
		return 0;
	}
	else {
		HANDLE file = CreateFileA(argv[1], GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_DELETE,
			NULL, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, NULL);
		if (file != INVALID_HANDLE_VALUE)
		{
			SECURITY_ATTRIBUTES sa = { sizeof(sa), &sd, FALSE };
			PACL pACL = BuildRestrictedSD(&sd);


			HANDLE hMapFile = CreateFileMappingA(file, &sa, PAGE_READONLY, 0, GetFileSize(file, 0), 0);
			if (hMapFile != INVALID_HANDLE_VALUE)
			{
				BYTE* pData = (BYTE*)MapViewOfFile(hMapFile, FILE_MAP_READ, 0, 0, 0);
				HANDLE file2;
				file2 = CreateFileA(outputfiles[0], FILE_GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
				if (file2 != INVALID_HANDLE_VALUE)
				{
					DWORD szWriten = 0;
					BOOL ret = WriteFile(file2, pData, UBOOTSECTIONSIZE, &szWriten, 0);
					outputlen[0] = UBOOTSECTIONSIZE;
					CloseHandle(file2);
				}
				
				file2 = CreateFileA(outputfiles[1], FILE_GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
				
				if (file2 != INVALID_HANDLE_VALUE)
				{
					DWORD szWriten = 0;
					image_header_t* pHeader = (image_header_t*)(pData + outputlen[0]);
					printf("uImage %s\n", pHeader->ih_name);
					printf("load %08x,entry %08x,size %08x\n", swapdword(pHeader->ih_load), swapdword(pHeader->ih_ep), swapdword(pHeader->ih_size));
					
					BOOL ret = WriteFile(file2, pData + outputlen[0], swapdword(pHeader->ih_size)+sizeof(image_header_t), &szWriten, 0);
					outputlen[1] += swapdword(pHeader->ih_size);
					CloseHandle(file2);
				}
				file2 = CreateFileA(outputfiles[2], FILE_GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
				if (file2 != INVALID_HANDLE_VALUE)
				{
					DWORD szWriten = 0;
					cramfs_super* pHeader = (cramfs_super*)(pData + RAMDSECTIONOFFSET);
					printf("ramdisk %s\n", pHeader->signature);
					printf("size %08x\n", pHeader->size);

					BOOL ret = WriteFile(file2, pData + RAMDSECTIONOFFSET, pHeader->size, &szWriten, 0);
					CloseHandle(file2);
				}
				UnmapViewOfFile(pData);
				CloseHandle(hMapFile);
			}
			CloseHandle(file);
		}

	}
	getchar();
	return 0;
}



static PACL BuildRestrictedSD(PSECURITY_DESCRIPTOR pSD)
{

	DWORD dwAclLength;
	PSID psidEveryone = NULL;
	PACL pDACL = NULL;
	BOOL bResult = FALSE;
	PACCESS_ALLOWED_ACE pACE = NULL;
	SID_IDENTIFIER_AUTHORITY siaWorld = SECURITY_WORLD_SID_AUTHORITY;
	SECURITY_INFORMATION si = DACL_SECURITY_INFORMATION;
	char errmsg[64];
	__try
	{

		//   initialize   the   security   descriptor   
		if (!InitializeSecurityDescriptor(pSD, SECURITY_DESCRIPTOR_REVISION))
		{
			sprintf_s(errmsg, 64, "InitializeSecurityDescriptor()   failed   with   error   %d\n", GetLastError());
			__leave;
		}

		//   obtain   a   sid   for   the   Authenticated   Users   Group   
		if (!AllocateAndInitializeSid(&siaWorld, 1,
			SECURITY_WORLD_RID, 0, 0, 0, 0, 0, 0, 0,
			&psidEveryone))
		{
			sprintf_s(errmsg, 64, "AllocateAndInitializeSid()   failed   with   error   %d\n", GetLastError());
			__leave;
		}

		//   NOTE:   
		//     
		//   The   Authenticated   Users   group   includes   all   user   accounts   that   
		//   have   been   successfully   authenticated   by   the   system.   If   access   
		//   must   be   restricted   to   a   specific   user   or   group   other   than     
		//   Authenticated   Users,   the   SID   can   be   constructed   using   the   
		//   LookupAccountSid()   API   based   on   a   user   or   group   name.   

		//   calculate   the   DACL   length   
		dwAclLength = sizeof(ACL)
			//   add   space   for   Authenticated   Users   group   ACE   
			+sizeof(ACCESS_ALLOWED_ACE)-sizeof(DWORD)
			+GetLengthSid(psidEveryone);

		//   allocate   memory   for   the   DACL   
		pDACL = (PACL)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwAclLength);
		if (!pDACL)
		{
			sprintf_s(errmsg, 64, "HeapAlloc()   failed   with   error   %d\n", GetLastError());
			__leave;
		}

		//   initialize   the   DACL   
		if (!InitializeAcl(pDACL, dwAclLength, ACL_REVISION))
		{
			sprintf_s(errmsg, 64, "InitializeAcl()   failed   with   error   %d\n", GetLastError());
			__leave;
		}

		//   add   the   Authenticated   Users   group   ACE   to   the   DACL   with   
		//   GENERIC_READ,   GENERIC_WRITE,   and   GENERIC_EXECUTE   access   
		if (!AddAccessAllowedAce(pDACL, ACL_REVISION,
			GENERIC_ALL,
			psidEveryone))
		{
			sprintf_s(errmsg, 64, "AddAccessAllowedAce()   failed   with   error   %d\n", GetLastError());
			__leave;
		}

		//   set   the   DACL   in   the   security   descriptor   
		if (!SetSecurityDescriptorDacl(pSD, TRUE, pDACL, FALSE))
		{
			sprintf_s(errmsg, 64, "SetSecurityDescriptorDacl()   failed   with   error   %d\n", GetLastError());
			__leave;
		}

		bResult = TRUE;

	}
	__finally
	{
		if (psidEveryone)
			FreeSid(psidEveryone);
	}

	if (bResult == FALSE)
	{
		if (pDACL) HeapFree(GetProcessHeap(), 0, pDACL);
		pDACL = NULL;
	}


	return  pDACL;
}